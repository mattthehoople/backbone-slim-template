var SignUpView = FormView.extend({
	
	events: {
		"click .send"	: "saveForm"
    },
    
    initialize: function () {
    	$('#fail').hide();
    }, 

	render: function() {
		$(this.el).html(this.template());
		return this;
	},	

	saveForm: function() {
		$('#fail').hide();

		var user = new Parse.User();
		user.set("username", $('#username').val());
		user.set("password", $('#password').val());
		user.set("email", $('#email').val());
		 
		user.signUp(null, {
		  success: function(user) {
		    window.location = 'index.html';
		  },
		  error: function(user, error) {
		    $('#fail').show();
		  }
		});

		return false;
	}
});