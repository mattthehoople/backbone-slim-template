window.App = Backbone.Router.extend({

    routes: {
        "": "home",
		"takings": "takings",
		"logoff": "logoff"
    },

    initialize: function () {
    	this.header = new Header();
        $('#header').html(this.header.render().el);

        this.footer = new Footer();
        this.footer.render();
		$("#footer").append(this.footer.el);
    },

    home: function () {
		$("#center").empty();
    var calender = new CalenderView();
    $('#center').append(calender.render().el);
	},

	takings: function () {
		$("#center").empty();
		var takings = new Takings({});
		var takingsView = new NewTakingsView({model:takings});
		$('#center').append(takingsView.render().el);
	},

	logoff: function () {

	}
});

window.LoginApp = Backbone.Router.extend({

    routes: {
        "": "home",
        "signup": "signup"
    },

    initialize: function () {

    },

    home: function () {
		$("#center").empty();
		var view = new LoginView();
        $('#center').append(view.render().el);
	},

	signup: function () {
		$("#center").empty();
		var view = new SignUpView();
        $('#center').append(view.render().el);
	}
});

templateLoader.load(["NewTakingsView", "Header", "Footer", "LoginView", "SignUpView", "CalenderView"],
	function () {

		if (Parse.User.current()) {
		  app = new App();
		} else {
		  app = new LoginApp();
		}


    	Backbone.history.start();
	}
);
